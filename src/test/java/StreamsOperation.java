import javafx.util.Pair;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.summingLong;
import static java.util.stream.Collectors.toList;

/**
 * Creation examples
 * +---------------------------------------------------------------+
 * | Colleactions.stream()                                         |
 * | Stream.of(T ... t)                                            |
 * | Stream.generate(Supplier<T> s)                                |
 * | Stream.concat(Stream<? extends T> a, Stream<? extends T> b)   |
 * | IntStream.range(int startInclusive, int endExclusive)         |
 * | IntStream.rangeClosed(int startInclusive, int endInclusive)   |
 * +---------------------------------------------------------------+
 *
 *
 * Intermediate Operations (all return Stream<T>)
 * +---------------+-----------------------+-------------------+
 * | Operation     | Type/fun. interface   | fun. description  |
 * +---------------+-----------------------+-------------------+
 * | filter        | Predicate<T>          | T -> boolean      |
 * | distinct      |                       |                   |
 * | skip          | long                  |                   |
 * | limit         | long                  |                   |
 * | map           | Function<T, R>        | T -> R            |
 * | flatMap       | Function<T, Stream<R>>| T -> Stream<R>    |
 * | sorted        | Comparator<T>         | (T,T) -> int      |
 *  +---------------+-----------------------+-------------------+
 *
 *
 * Terminal Operations
 * +---------------+-----------------------+---+---------------+---------------+
 * | Operation     | Type/fun. interface   | fun. description  | return type   |
 * +---------------+-----------------------+-------------------+---------------+
 * | anyMatch      | Predicate<T>          | T -> boolean      | boolean       |
 * | noneMatch     | Predicate<T>          | T -> boolean      | boolean       |
 * | allMatch      | Predicate<T>          | T -> boolean      | boolean       |
 * | findAny       |                       |                   | Optional<T>   |
 * | findFirst     |                       |                   | Optional<T>   |
 * | forEach       | Consumer<T>           | T -> void         | void          |
 * | collect       | Collector<T, A, R>    |                   | R             |
 * | reduce        | BinaryOperator<T>     | (T, T) ->         | Optional<T>   |
 * | count         | long                  |                   |               |
 * +---------------+-----------------------+-------------------+---------------+
 */

public class StreamsOperation {

    @AllArgsConstructor
    @Getter
    public static class OrderEntity {
        private String orderRef;
        private List<ItpEntity> itps;

        @Override
        public String toString() {
            return "{"
                    + "\n\t\"orderRef\": \"" + orderRef + "\","
                    + "\n\t\"itps\": [" + itps.stream()
                    .map(ItpEntity::toString)
                    .collect(joining(", "))
                    .replace("\n", "")
                    .replace("\t", " ") + "]"
                    + "\n}";
        }
    }

    @AllArgsConstructor
    @Getter
    public static class ItpEntity {
        private Long amount;
        private String itpStatus;

        @Override
        public String toString() {
            return "{"
                    + "\n\t\"amount\": " + amount + ","
                    + "\n\t\"itpStatus\": \"" + itpStatus + "\""
                    + "\n}";
        }
    }



    /**
            {
                "orderRef": "Order 1",
                "itps": [{ "amount": 10, "itpStatus": "OK"}]
            }
            {
                "orderRef": "Order 2",
                "itps": [{ "amount": 20, "itpStatus": "ERROR"}, { "amount": 30, "itpStatus": "OK"}]
            }
     */
    private static Stream<OrderEntity> getOrders() {
        Stream<OrderEntity> streamOfEntities = Stream.of(1, 2)
                .map(e -> new OrderEntity("Order " + e,
                        LongStream.range(e, e * 2)
                                .mapToObj(i -> new ItpEntity(i * 10, i % 2 != 0 ? "OK" : "ERROR"))
                                .collect(toList())));
//                .peek(System.out::println);

        return streamOfEntities;
    }



    @Test
    public void map_countItps() {
        Stream<OrderEntity> streamOfOrders = getOrders();

        long result = streamOfOrders
                .map(OrderEntity::getItps)
                .map(List::size)
                .count();

        System.out.println(result);
        Assertions.assertEquals(3, result);
    }



    @Test
    public void flatMap_filter_countItps_withStatusOK() {
        Stream<OrderEntity> streamOfOrders = getOrders();

        long result = streamOfOrders
                .map(OrderEntity::getItps)
                //List: { "amount": 10, "itpStatus": "OK"}
                //List: { "amount": 20, "itpStatus": "ERROR"}, { "amount": 30, "itpStatus": "OK"}
                .flatMap(List::stream)
                //Stream: { "amount": 10, "itpStatus": "OK"}, { "amount": 20, "itpStatus": "ERROR"}, { "amount": 30, "itpStatus": "OK"}
                .filter(e -> "OK".endsWith(e.getItpStatus()))
                .count();

        System.out.println(result);
        Assertions.assertEquals(2, result);
    }



    @Test
    public void filter_orders_withFailedItps() {
        Stream<OrderEntity> streamOfOrders = getOrders();

        List<OrderEntity> result = streamOfOrders
                .filter(o -> o.getItps()
                        .stream()
                        .map(ItpEntity::getItpStatus)
                        .anyMatch("ERROR"::equals))
                .collect(toList());

        result.forEach(System.out::println);
        Assertions.assertEquals(1, result.size());
    }



    @Test
    public void collect_count_ItpByStatus() {
        Stream<OrderEntity> streamOfOrders = getOrders();

        Map<String, Long> collect = streamOfOrders
                .map(OrderEntity::getItps)
                .flatMap(List::stream)
                .collect(groupingBy(ItpEntity::getItpStatus, counting()));

        System.out.println(collect);
    }



    @Test
    public void collect_sum_amount_byOrderRef() {
        Stream<OrderEntity> streamOfOrders = getOrders();

        Map<String, Long> collect = streamOfOrders
                .flatMap(o -> o.getItps().stream()
                                            .map(i -> new Pair<String, Long>(o.getOrderRef(), i.getAmount())))
                .collect(groupingBy(Pair::getKey, summingLong(Pair::getValue)));

        System.out.println(collect);
    }



    @Test
    public void reduce_sum_amount() {
        Stream<OrderEntity> streamOfOrders = getOrders();

        long result = streamOfOrders
                .map(OrderEntity::getItps)
                .flatMap(List::stream)
                .mapToLong(ItpEntity::getAmount)
                .reduce(0, (sum, current) -> sum += current);

        System.out.println(result);
    }



    @Test
    public void collect_itp_with_biggest_amount_withStatusOK() {
        Stream<OrderEntity> streamOfOrders = getOrders();

        Optional<ItpEntity> max = streamOfOrders
                .map(OrderEntity::getItps)
                .flatMap(List::stream)
                .filter(i -> "OK".endsWith(i.getItpStatus()))
                .max(comparing(ItpEntity::getAmount));

        System.out.println(max.get());
    }
}
