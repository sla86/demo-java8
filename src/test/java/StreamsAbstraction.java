import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.List;
import java.util.function.Function;
import java.util.function.IntConsumer;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class StreamsAbstraction {

    @Test
    public void infinit_stream() {
        Stream<Instant> infinitStream = Stream.generate(Instant::now);

        infinitStream.forEach(System.out::println);
    }

    @Test
    public void declarative_stream() {
        IntStream streamOfNumbers = IntStream.range(0, 10)
                .filter(e -> {System.out.println("filter \t" + e); return e % 2 == 0;})
                .map(e -> {System.out.println("map \t" + e); return e * 2; })
                .sorted();

        System.out.println("After Stream declaration\n\n");

        streamOfNumbers.forEach(System.out::println);
    }

    @Test
    public void stream_from_file() throws IOException {
        Stream<String> lines = Files.lines(Paths.get(".\\src\\test\\resources\\someFile.txt"));

        lines.map(String::toUpperCase)
                .forEach(System.out::println);
    }

    @Test
    public void parallel_stream() {
        IntConsumer intSleepConsumer = (s) -> {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        int sum = IntStream.of(1, 2, 3)
                .parallel()
                .peek(intSleepConsumer)
                .sum();

        System.out.println(sum);
    }

}
