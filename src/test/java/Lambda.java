import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Lambda {

    @AllArgsConstructor
    @ToString
    @Getter
    private class Apple {
        private String collor;
        private Integer weight;
    }

    @Test
    public void show_Old_Comparator() {
        List<Apple> inventory = Arrays.asList(new Apple("red", 100), new Apple("green", 70));

        Collections.sort(inventory, new Comparator<Apple>() {
            public int compare(Apple a1, Apple a2) {
                return a1.getWeight().compareTo(a2.getWeight());
            }
        });

        System.out.println(inventory);
    }
}
