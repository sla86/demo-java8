# java8-demo

##labda
Today we are going to discuss about some of feature that where introduced in java 8.
Despite that the release of java 8 was done in 2014, and now, after 5 years, we have already java 11 as LTS release,
this topic is still actual for discussions, debates, trainings and arguments in Pull Request comments.

The addition of functional behavior in java is considered one of the biggest and most important changes for the entire java ecosystem.
Let’s think about "why some java developer were so eager for lambda support" because this is considered to be the main driver for all of the following new features,
and what is the difference between functional and OOP paradigms, and what benefits brings functional behavior for java.

In order to answer these questions, I'm going to show and practical example.
I have a class "Apple" which is the domain model of my example. This class has 2 attributes, String color, Integer weight.
Now I want to sort a collection of apples which is pretty common use case in our day by day work.

Let's do a detailed analyses of this example.
We are already lucky that we don't have to implement any kind of soring algorithms with array parsing and manipulation like I was doing it back in the school days
The programing language is already helping us with some partial solutions.

So we have a utility class Collections, with a static method sort.
Method sort takes a list, inventory in our case, and a type defined by the interface Comparator,
where is defined an abstract method "compare" which we actually have to implement in order to provide the sorting condition.

Now let’s extract this comparator for a closer look.

Technically - this construction is nothing else but an anonymous inner class. 
usage of anonymous inner class, is an ability to create on the fly implementations of the most commonly used interfaces,
where only few lines of code are required in order to satisfy the requirements.

Logically - this is an implementation of Strategy design pattern. 
This patter itself is possible because of one particular ability of the object oriented languages which is Polymorphism.
And this is the best implementation of sorting a collection which could be done till Java 7.

By calling it "the best implementation till Java 7" I want to emphasize that,
in Java 8 it can be done much better because of its functional behaving.

Let’s see now how we can transform this Object Oriented implementation into a "more" functional code. 
First, let's remove everything that is suggesting that we are having an object here,
The new keyword defenetly can be removed since this is the instruction to create an object.
The class declaration can also be removed because by definition a class is a templatate of an object. 

So, we are left with the function itself. 
We don't need the access modifier, the return type, and the name of the function, since these are defined in the interface.
Do we need the input arguments? yes wee need, if we want to use them we have to know how to reference them.
Now the only remaining thing, to make it work, is to use the lambda sign to tell that this is the implementation of a function,
where the left part is the input and the right side is the body of the function.

Here we are with our first lambda exprection, all the code that was removed so far - is actually deduced from the interface.
By the way who can tell what is a functional interface?
...


But we don't have to specefy the type, sice in can be deduced from the reference.
Labda constuction has a nice feature, if the method is a one-liner, then we can also remove the curly brackets and the return statement 

Now this implementation looks slightly betteer.
Technically - we have now only the benefit of replacing the anonimous class with a labla and as a result we have slightly less code
Logically - this is the same mix of procedural and Object oriented paradigms. So lets try to change it to a more functional implementation

Lets get rid of this static utility class, and as altenative we can call method sort on inventory directly. [remove]
If we check the java doc, we can see that this method was also added in java 8
Now we can use a new method in Comparator.comparing and pass a method reference wich should be applied doring coparison.
  

TODO: review all functional interfaces

with your permission, I'll like to show some examples in our codebase
BlacklistIpOaqServiceImpl:77
KafkaManagerServiceImpl:57
RefundRequestDTOToExceptionDBOMapper:45
DBAccessOperationHelper:744
CupInfoValidator:25

##stream
The definition in the books is:
Streams Abstraction, are an update to the Java 8 that lets you manipulate sequence of data in a declarative way.
(you express a query rather than code an ad hoc implementation for it).
Practical definition is:
<br>At a junior developer level stream API is something that makes life easyer while processing collection.
<br>At a senior developer level stream API is an abstraction of data processing. 
Some similar abstraction you cand find in reactive java which is already an arhitectural pattern and cancear,
also similar abstraction can be foun in enterprise integration tools like apache cammel.

Check the image

infinit_stream:
Strim is an API is not a collection, it doesn't know allot about eleme types and size of elements.
So we can process any kind of elements in infinit number. 

declarative_stream
Declarative API allows us to decouple the description of the process from the execution.
So we can describe how we want to process our elements and use this pipeline later.
Technically the streams are lazy and are executed only terminal operations are executed.

stream_from_file
A source of stream doesn't always have to be a collection, It can be any external resource.

parallel_stream
Stream API allows us to define pipeline of data processing which can be executed in multi threaded context without any additional overhead 